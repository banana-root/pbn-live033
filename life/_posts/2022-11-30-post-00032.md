---
layout: post
title: "11. 7. 청약! 포레나 평택화양! 전국청약, 등기전 전매가능, 중도금..."
toc: true
---


## 포레나 평택화양 청약일정 공급규모 분양가 중도금대출 평면도 입지환경 화양지구

### 목차
 ◎ 포레나 평택화양 청약일정

 ◎ 포레나 평택화양 공급규모

 ◎ 포레나 평택화양 평면도

 ◎ 포레나 평택화양 분양가
 ◎ 포레나 평택화양 청약사항

 ◎ 포레나 평택화양 특별공급

 ◎ 모집공고/분양홈페이지

 안녕하세요. 청정남입니다. 오늘은 경기도 평택시 현덕면 화양리 평택 화양지구 도시개발사업 7-2BL에 공급되는 "포레나 평택화양" 입주자모집공고 및 청약일정 곰곰이 살펴보겠습니다. 포레나 평택화양은 아파트 지하 2층, 땅덩이 29층, 10개 동, 총 995세대 규모로 조성되며 입주시기는 2025년 11월 예정입니다.
 

 서평택 대규모 주거타운이 형성되는 화양지구 도시개발사업은 여의도 면적과 흡사한 279만여 평의 미니신도시급으로 계획인구만 5만 그대 명이 넘기 왜냐하면 고덕국제신도시와 맞먹는 지역으로 성장할 것으로 기대를 모으고 있습니다. 주거, 문화, 영업 등 일통 기능을 갖춘 개발 사업으로 학교부터 상권까지 인프라가 부족함 궁핍히 조성되는 특징을 갖추고 있으며 여기에 서해선 복선전철 개통 및 경부선 직결로 서해안 거점도시로 거듭날 준비를 하고 있네요. 한갓 부근지 현화지구의 신수 상업시설을 이용하는데 불편함이 없으며 2025년 말에는 복합공공청사인 관심 출장소가 건립될 예정으로 부가적으로 수반되는 밑받침 시설들이 다양하게 구축될 것으로 예상됩니다.
 유달리 포레나 평택화양은 초등학교를 품은 초품아 지역 예정이며, 이외에도 반경 1km 내에 초, 중, 고교 개교 예정인 곳이 7곳에 이르러 도보통학이 가능한 학세권을 형성하겠습니다. 아울러 중앙공원, 종합병원 의료시설, 중심상업시설 등이 골고루 인근에 갖춰질 예정입니다. 교통환경은 차기 서부내륙, 서해안, 파주 고속도로가 연계될 것으로 보이며 2023년 서해선 복선 석현 개장 및 노농 고속선 KTX 직결로 광역 접근성이 크게 향상될 전망입니다.
 

 만근 부동산 및 청약시장의 관심도가 떨어지면서 역시나 평택화양지구도 고전을 면치 못하고 있습니다. 당연한 것으로 보이며 미리미리 분양한 단지인 "평택화양 휴먼빌 퍼스트시티", "e편한세상 평택 라씨엘로", "e편한세상 평택 하이센트" 일체 미달물량이 대다수 발생한 점을 봤을 단시 '포레나 평택화양' 과약 다수의 미흡 물량이 나올 것으로 보입니다. 평택 화양지구 아직은 시각 일층 지켜봐야 할 곳으로 보이네요.
 

 평택시 및 전국에 거주하는 자에게 [평택 화양 서희스타힐스](http://pt-starhills.co.kr) 청약의 기회가 주어지며 전매제한은 소유권이전등기일까지, 실거주의무는 없으며 중도금대출은 중도금무이자 조건으로 60% 범위에서 가능합니다. 더구나 본 아파트는 비투기과열지구 및 비청약과열지역의 민간택지에서 공급하는 분양가상한제 미적용 민영주택으로 당첨자로 선정 시 재당첨제한을 적용받지 않고, 기존 살림집 당첨 여부와 상관없이 본 아파트 청약이 가능합니다.

 한결 빨라지는 교통환경
 서해선-KTX 안중역(예정) 연결
 6차선 일거리 건설 계획, 서해안고속도로 포승IC 및 서부내륙고속도로 예정
 

 한결 풍요로운 중심생활
 지대 주변 초등학교&중심상업지구 계획, 종합병원(예정)
 도서관, 체육시설, 문화공간을 갖춘 복합공공청사 계획
 

 더욱 여유로운 직주근접
 원정지구 국가산업단지, 포승 국가산업단지, 평택항만
 포승2일반산업단지 등과 인접한 생령 뒤쪽 주거타운
 

 한층 높아지는 미래가치
 경기경제자유구역 포승지구 및 현덕지구 개발호재
 평택호 관광단지, 평택항 등 물류&관광 중심지 육성
 

### 포레나 평택화양 청약일정

 특별공급 : 22. 11. 7. (월)
 일반공급 1순위 : 22. 11. 8. (화)
 일반공급 2순위 : 22. 11. 9. (수)
 당첨자발표 : 22. 11. 15. (화)
 청약방법 : 사업주체 견본주택 내지 한국부동산원 청약홈
 ※사업주체 견본주택 : 경기도 평택시 안중읍 송담리 856-1
 

### 포레나 평택화양 공급규모
 공급위치 : 경기도 평택시 현덕면 화양리 평택 화양지구 도시개발사업 7-2BL
 공급규모 : 아파트 지하 2층, 세계 29층, 10개 동, 총 995세대[특별고급 456세대, 일반공급 539세대]
 입주시기 : 2025년 11월 예정

 

### 포레나 평택화양 평면도

 

### 포레나 평택화양 분양가
 74A타입(78세대) : 4.01억
 74B타입(75세대) : 4.10억
 74C타입(75세대) : 4.05억
 74D타입(53세대) : 4.01억
 84A타입(205세대) : 4.63억
 84B타입(363세대) : 4.60억
 99A타입(146세대) : 5.46억

 포레나 평택화양 분양가 납부는 계약금 10%, 중도금 60%, 잔금 30%로 납부가능하며, 발코니 확장 공사비는 계약금 10%, 잔금 90%로 납부가능합니다.

 

### 포레나 평택화양 청약사항
 ◎ 지역구분
 비투기과열지구 및 비청약과열지역, 분양가상한제 미적용 민영주택
 ◎ 공급대상
 평택시 및 전국에 거주하는 만 19세 이상인 자제 내지 세대주인 미성년자
 단, 등수 기수 경쟁이 있을 우여곡절 평택시 6개월 가 거주자 우선

 ◎ 일반공급 1순위 요건
 - 청약통장 가입기간 12개월 유동 및 예치금기준 충족
 ◎ 일반공급 1순위 당첨비율
 전용85이하 가점제 40%, 추첨제 60%
 전용85초과 추첨제 100%
 ◎ 당첨일로부터 향후 5년간 투기과열지구 및 청약과열지역에서 공급하는 주택의 1순위 청약접수 제한
 ◎ 전매제한 : 소유권이전등기일까지
 ◎ 실거주의무 : 없음
 ◎ 중도금대출 : 중도금무이자, 총 공급대금의 60%
 

### 포레나 평택화양 특별공급
 ◎ 기관추천 특별공급[82세대]
 - 입주자모집공고일 이적 무주택세대구성원으로서 관계 기관의 추천을 받은 자
 - 청약통장 가입기간 6개월 유동 및 지역별/면적별 예치기준금 충족
 

 ◎ 다자녀 특별공급[96세대]
 - 입주자모집공고일 즉금 평택시 및 전국에 거주하는 만 19세 미만의 이세 3명(태아포함)을 둔 무주택세대구성원
 - 청약통장 가입기간 6개월 흐름 및 지역별/면적별 예치기준금 충족
 

 ◎ 신혼부부 특별공급[169세대]
 - 입주자모집공고일 현재 평택시 및 전국에 거주하는 혼인기간 7년 이내인 무주택세대구성원
 - 청약통장 가입기간 6개월 조류 및 지역별/면적별 예치기준금 충족
 - 전년도 도시근로자 월평균소득 기준의 140% 이하인 자 (맞벌이 160% 이하)
 

 ◎ 노부모부양 특별공급[27세대]
 - 입주자모집공고일 막 평택시 및 전국에 거주하는 만 65세 이상의 직계존속을 3년 몽상 계절 부양하고 있는 무주택세대주
 - 청약통장 가입기간 12개월 리듬 및 지역별/면적별 예치기준금 충족
 - 과일 5년 제대로 당첨된 생각 세대에 속하지 않을 것
 

 ◎ 생애최초 특별공급[82세대]
 - 입주자모집공고일 중제 평택시 및 전국에 거주하는 생애최초로 주택을 구입하는 무주택세대구성원
 - 청약통장 가입기간 12개월 흐름 및 지역별/면적별 예치기준금 충족
 - 전년도 도시근로자 월평균소득 기준의 160% 이하인 자
 - 구시 5년 곧장 당첨된 의미 세대에 속하지 않을 것
 - 입주자모집공고일 방금 성가 중이거나 미혼인 자녀가 있는 글씨 혹은 1인 가구
 

### 모집공고/분양홈페이지

 포레나 평택화양 청약관련하여 추가선택품목 및 계약시 유의사항 등은 입주자모집공고문 확인 바라며 근체 요식 홈페이지에서 e모델하우스 등 참고 바랍니다.
 

### 채널안내/유의사항
 기수 인가 마련을 위한 첫단계! 부동산 청약은 당첨확률이 희박하다?, 어렵고 복잡하다? 조금만 관심가지면 여러분들도 당첨될 이운 있습니다.

 부동산청약, 줍줍청약요약, 부동산리뷰 등 다양한 정보 확인하고 청약에 당첨되시길 진심으로 바랍니다.

 청정남(청약정리하는남자) 유튜브
 리치몬드 경제이야기
 ※ 부동산청약 및 투자 등에 관련하여 핵심적인 사항, 개인적인 의견을 작성한 것이니 청약, 투자 등의 책에 있어서 투자자 본인에게 있음을 알려드립니다. 입주자모집공고 및 분양홈페이지를 통해 작성한 것이니 다음 수정사항 있으면 참고하고 수정하도록 하겠습니다.
